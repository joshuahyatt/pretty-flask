from flask_wtf import FlaskForm
from wtforms import *

class FieldGenerator():
    def __init__(self, fields):
        self.formfields = {}
        for field in fields:
            options = self.get_options(field)
            f = getattr(self, "create_field_for_"+field['type'] )(field, options)
            self.formfields[field['name']] = f
        self.formfields["submit"] = SubmitField("Submit")

    def get_options(self, field):
        options = {}
        options['label'] = field['label']
        options['description'] = field.get("description", None)
        options['default'] = field.get("default", None)
        options['validators'] = field.get("optional", [validators.required()])
        return options

    def create_field_for_text(self, field, options):
        #options['max_length'] = int(field.get("max_length", "20") )
        return StringField(**options)

    def create_field_for_integer(self, field, options):
        #options['max_value'] = int(field.get("max_value", "999999999") )
        #options['min_value'] = int(field.get("min_value", "-999999999") )
        return IntegerField(**options)

    def create_field_for_radio(self, field, options):
        options['choices'] = [ (choice['value'], choice['name'] ) for choice in field['choices'] ]
        return RadioField(**options)

    def create_field_for_select(self, field, options):
        options['choices']  = [ (choice['value'], choice['name'] ) for choice in field['choices'] ]
        return SelectField(  **options)

    def create_field_for_checkbox(self, field, options):
        return BooleanField(**options)

    def create_field_for_file(self, field, options):
        return FileField(**options)
