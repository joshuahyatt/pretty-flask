import logging
from functools import *
import networkx as nx
from multiprocessing import Pool
import rapydminer as rm

def drop_null(data_frame):
    return data_frame.dropna()

def get_metrics(y_true, y_predicted):
    metrics = [accuracy_score,
    adjusted_mutual_info_score,
    adjusted_rand_score,
    auc,
    average_precision_score,
    balanced_accuracy_score,
    brier_score_loss,
    calinski_harabaz_score,
    check_scoring,
    classification_report,
    cluster,
    cohen_kappa_score,
    completeness_score,
    confusion_matrix,
    consensus_score,
    coverage_error,
    davies_bouldin_score,
    euclidean_distances,
    explained_variance_score,
    f1_score,
    fbeta_score,
    fowlkes_mallows_score,
    hamming_loss,
    hinge_loss,
    homogeneity_completeness_v_measure,
    homogeneity_score,
    jaccard_similarity_score,
    label_ranking_average_precision_score,
    label_ranking_loss,
    log_loss,
    matthews_corrcoef,
    mean_absolute_error,
    mean_squared_error,
    mean_squared_log_error,
    median_absolute_error,
    mutual_info_score,
    normalized_mutual_info_score,
    precision_recall_curve,
    precision_recall_fscore_support,
    precision_score,
    r2_score,
    recall_score,
    roc_auc_score,
    roc_curve,
    silhouette_samples,
    silhouette_score,
    v_measure_score,
    zero_one_loss]
    results = {}
    for metric in metrics:
        try:
            results[metric.__name__] = metric(y_true,y_predicted)
        except:
            continue
    return results

def fit(func, *args, **kwargs):
    return func.fit(*args, **kwargs)

def score(func, *args, **kwargs):
    return func.score(*args, **kwargs)

def key_split(data_frame, keys):
    return data_frame[keys],data_frame.drop(keys, axis=1)

def concat_frames(*args, **kwargs):
    frames = list(args)
    return pd.concat(frames, join_axes=[frames[0].index], **kwargs)

def frame_it(array, **kwargs):
    return pd.DataFrame(array, **kwargs)

def call(f, *args, **kwargs):
    return f(*args, **kwargs)

def index_split(data_frame):
    return data_frame.index

def stop(*args, **kwargs):
    return

def index(enumerated,index):
    return enumerated[index]

def fit_transform(func, *args, **kwargs):
    if hasattr(func, "fit_transform"):
        return func.fit_transform(*args, **kwargs)
    elif hasattr(func, "fit") and hasattr(func, "transform"):
        func.fit(*args, **kwargs)
        return func.transform(data)
    else:
        return func(**kwargs)

def fit_predict(func, train, labels, test, **kwargs):
    assert hasattr(func, "fit")
    assert hasattr(func, "predict")
    func.fit(train, labels, **kwargs)
    return func.predict(test)

def transform(func, *args, **kwargs):
    if hasattr(func, "transform"):
        return func.transform(*args, **kwargs)
    elif hasattr(func, "predict"):
        return func.predict(*args, **kwargs)
    else:
        return func(*args, **kwargs)

predict = transform

def build_model(data):
    input = Input(shape=(data.shape[1],))
    model = rm.Model(input, rm.simple_autoencoder(37,input))
    model.compile(optimizer='adadelta', loss='binary_crossentropy')
    #fit_params = {"epochs":200, "batch_size":100}
    #model.fit(data, data, **fit_params)
    return model

logging.basicConfig()
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

def topological_sort(data):
    """code from pyungo library, that computes a topological order to execute in parallel
    """
    for key in data:
        data[key] = set(data[key])
    for k, v in data.items():
        v.discard(k)  # ignore self dependencies
    extra_items_in_deps = reduce(set.union, data.values()) - set(data.keys())
    data.update({item: set() for item in extra_items_in_deps})
    while True:
        ordered = set(item for item,dep in data.items() if not dep)
        if not ordered:
            break
        yield sorted(ordered)
        data = {item: (dep - ordered) for item,dep in data.items()
                if item not in ordered}

def subgraph(graph, vertices):
    LOGGER.disabled = True
    new_graph = Graph()
    for id in vertices:
        new_graph._add_vertex(graph.vertices[id])
    LOGGER.disabled = False
    return new_graph

def run_graph(graph, pool_size = 2, vertices = None, include_keys=False, **kwargs):
    """ Executes a graphs flow with specified pool number if the graph's parallel flag is enabled. If vertices are supplied, will execute the induced subgraph of those vertices.
    """
    if vertices is None:
        vertices = graph.vertices.keys()
        if len(vertices) == 0:
            if include_keys:
                return {}
            else:
                return
    assert len(vertices) > 0, "An empty list was specified for vertices, please use None type instead"
    #Initialize values
    for (id, value) in kwargs.items():
        try:
            graph.vertices[id].value = value
        except:
            #Need to do better than this with exception catching
            LOGGER.warning('Ignoring vertex "{}", either the graph could not find the vertex or set the value'.format(id))

    execution_graph = subgraph(graph, vertices)

    #Run Sub Graph
    if len(graph.vertices) == 0:
        return {}
    elif graph.parallel:
        LOGGER.info('Running graph in parallel mode.')
        ranked_dependencies = {node:list(set(execution_graph.predecessors(node))) for node in execution_graph.nodes}
        for level in topological_sort(ranked_dependencies):
            #Parallel options here
            pool = Pool(pool_size)
            results = pool.map(graph.call_vertex, [graph.vertices[id] for id in level])
            pool.close()
            pool.join()
            for dictionary in results:
                if len(dictionary) == 0:
                    continue
                for (id, value) in dictionary.items():
                    graph.vertices[id].value = value
    else:
        LOGGER.info('Running graph in serial mode.')
        for id in list(nx.topological_sort(execution_graph)):
            dictionary = graph.call_vertex(graph.vertices[id])
            if len(dictionary) == 0:
                continue
            for (id, value) in dictionary.items():
                graph.vertices[id].value = value
    keys = []
    values = []
    for id in execution_graph.nodes:
        if execution_graph.out_degree(id)==0 and graph.vertices[id].type == "Object":
            keys.append(id)
            values.append(graph.vertices[id].value)
    graph._gb_collect()
    if include_keys:
        return {k:v for k,v in zip(keys,values)}
    else:
        if len(values) == 0:
            return
        elif len(values) == 1:
            return values[0]
        else:
            return tuple(values)

def run_view(graph_multi_view, pool_size = 2, include_keys=False, view = "master", **kwargs):
    assert view in graph_multi_view.views, '"{}" is not a view of this graph'.format(view)
    vertices = graph_multi_view.views[view]
    return run_graph(graph_multi_view, pool_size, include_keys = include_keys, vertices = vertices, **kwargs)

train_view = partial(run_view, view="train")
train_view.__name__ = "train_view"
test_view = partial(run_view, view="test")
test_view.__name__ = "test_view"
predict_view = partial(run_view, view="predict")
predict_view.__name__ = "predict_view"

class Vertex:
    """ Base class for defining a vertex on a graph.
    """
    def __init__(self, id, type):
        assert isinstance(id, str)
        self.id = id
        self.type = type

class FuncVertex(Vertex):
    """ A Vertex on a Graph that stores a function, it's args and kwargs in reference to other vertices and its outputs in reference
    other vertices.
    """
    def __init__(self, func, in_args, out, in_kwargs = None, id=""):
        if id == "":
            Vertex.__init__(self, func.__name__, "Function")
        else:
            Vertex.__init__(self, id, "Function")
        self.func = func
        self.in_args = in_args if in_args else []
        self.in_kwargs = in_kwargs if in_kwargs else {}
        self.out = out

class ObjVertex(Vertex):
    """ A Vertex on a Graph that stores a value. After execution the value is deleted by garbage collection
    unless the cache flag is present
    """
    def __init__(self, id, value, cache):
        Vertex.__init__(self, id, "Object")
        self.value = value
        self.cache = cache

class Graph(nx.DiGraph):
    """ Graph consists of vertices along with a simple interface to add and delete them. The graph is
    the single object that represents the flow of a program. Graph class expands upon the networkx DiGraph
    to build directional acyclical graphs.
    """
    def __init__(self, parallel = True):
        self.vertices = {}
        self.parallel = parallel
        nx.DiGraph.__init__(self)
    def __str__(self):
        return nx.to_pandas_adjacency(self).__str__()

    def _remove_parameter(self, parameter, id):
        if isinstance(self.vertices[id], ObjVertex):
            return
        if parameter in self.vertices[id].in_args:
            args = list(self.vertices[id].in_args)
            args.remove(parameter)
            args = tuple(args)
            self.vertices[id].in_args = args
        if parameter in self.vertices[id].in_kwargs.values():
            dictionary = {k:v for k,v in self.vertices[id].in_kwargs.items() if not v}
            self.vertices[id].in_kwargs = dictionary
        if parameter in self.vertices[id].out:
            self.vertices[id].out.remove(parameter)


    def _remove_vertex(self, vertex):
        for id in list(self.successors(vertex.id)) + list(self.predecessors(vertex.id)):
            self._remove_parameter(vertex.id, id)
        del self.vertices[vertex.id]
        self.remove_node(vertex.id)
        assert nx.is_directed_acyclic_graph(self)==True, "This graph now contains loops, which are invalid"

    def remove_vertex(self, id):
        self._remove_vertex(self.vertices[id])

    def _add_vertex(self, vertex):
        if vertex.id in self.vertices:
            LOGGER.info('vertex "{}" already exists in the graph, no overwrite made'.format(vertex.id))
        else:
            self.vertices[vertex.id]=vertex
            if isinstance(vertex, FuncVertex):
                for arg in vertex.in_args:
                    try:
                        self.add_obj(arg, cache = False)
                    except:
                        assert arg in self.vertices
                    self.add_edge(arg, vertex.id)
                for kwarg in vertex.in_kwargs.values():
                    try:
                        self.add_obj(kwarg, cache = False)
                    except:
                        assert kwarg in self.vertices
                    self.add_edge(kwarg, vertex.id)
                for output in vertex.out:
                    try:
                        self.add_obj(output, cache = False)
                    except:
                        assert output in self.vertices
                    self.add_edge(vertex.id,output)
            else:
                self.add_node(vertex.id)

        if nx.is_directed_acyclic_graph(self)!=True:
            LOGGER.warning('This graph now contains a loops, which is invalid, deleting vertex "{}" to resolve.'.format(vertex.id))
            self._remove_vertex(vertex)

    def add_func(self, func, in_args, output = None, in_kwargs = None, id=""):
        if output is None:
            output = []
        if in_kwargs is None:
            in_kwargs = {}
        vertex = FuncVertex(func, in_args, output, in_kwargs, id)
        self._add_vertex(vertex)

    def add_obj(self, id, value=None, cache=True):
        vertex = ObjVertex(id, value, cache)
        self._add_vertex(vertex)

    def _realize_args(self, vertex):
        """ returns the args and kwargs for a vertex using the values of the vertices
        specified in in_args and in_kwargs
        """
        assert isinstance(vertex, FuncVertex), 'Vertex "{}" is not of type FuncVertex.'.format(vertex.id)
        args = (self.vertices[id].value for id in vertex.in_args)
        kwargs = {kwarg:self.vertices[vertex.in_kwargs[kwarg]].value for kwarg in vertex.in_kwargs}
        return args, kwargs

    def _clean(self, id):
        assert self.vertices[id].type == "Object"
        self.vertices[id].value = None

    def _gb_collect(self):
        dirty_vertices = [id for id in self.vertices if self.vertices[id].type == "Object" and not self.vertices[id].cache]
        for id in dirty_vertices:
            self._clean(id)

    def merge(self, graph):
        for id in graph.vertices:
            self._add_vertex(graph.vertices[id])

    def call_vertex(self, vertex):
        """ The heart of graph object. This function takes a vertex and executes the function on its
        dependent vertices specified in the in_args and in_kwargs parameters. The outputs of the function
        are then mapped to the corresponding vertices specified in outputs.
        """
        LOGGER.debug('Currently execting "{}"'.format(vertex.id))
        #possibly release memory here
        if isinstance(vertex, ObjVertex):
            assert vertex.value is not None, 'Missing value for vertex "{}"'.format(vertex.id)
            return {}
        assert isinstance(vertex, FuncVertex), 'Vertex "{}" is not of type FuncVertex.'.format(vertex.id)
        #call function
        args, kwargs = self._realize_args(vertex)
        outputs = vertex.func(*args, **kwargs)

        if len(vertex.out)>1:
            try:
                return {k:v for (k,v) in zip(vertex.out, list(outputs))}
            except:
                raise Exception('Output of "{}" does not match expected graph'.format(vertex.id))
        else:
            if len(vertex.out) == 0:
                return {}
            else:
                return {vertex.out[0]:outputs}

class GraphMultiView(Graph):
    def __init__(self, parallel = True):
        Graph.__init__(self, parallel = parallel)
        self.views = {"master":[]}

    def __str__(self):
        return "Graph: \n" + Graph.__str__(self) + "\n \nviews:\n" + self.views.__str__()

    def add_view(self, name, vertices = None):
        if vertices is None:
            vertices = []
        self.views[name] = vertices

    def remove_view(self, name):
        if name in self.views:
            del self.views[name]
        else:
            LOGGER.info('"{}" is not a part of this graph, no options removed'.format(name))

    def remove_from_view(self, id, view):
        if id in self.views[view]:
            self.views[view].remove(id)

    def add_to_view(self, id, view):
        if id not in self.views[view]:
            self.views[view].append(id)

    def remove_vertex(self, id):
        self._remove_vertex(self.vertices[id])
        for view in self.views.keys():
            self.remove_from_view(id, view)

    def add_func(self, func, in_args, output = None, in_kwargs = None, id="", view = "master"):
        if output is None:
            output = []
        if in_kwargs is None:
            in_kwargs = {}
        pre_nodes = set(self.nodes)
        vertex = FuncVertex(func, in_args, output, in_kwargs, id)
        self._add_vertex(vertex)
        post_nodes = set(self.nodes)
        for node in post_nodes-pre_nodes:
            self.add_to_view(node, "master")
            self.add_to_view(node, view)

    def add_obj(self, id, value=None, cache=True, view = "master"):
        vertex = ObjVertex(id, value, cache)
        self._add_vertex(vertex)
        self.add_to_view(id, "master")
        self.add_to_view(id, view)

    def merge(self, graph, view = "master"):
        pre_nodes = set(self.nodes)
        for id in graph.vertices:
            self._add_vertex(graph.vertices[id])
        post_nodes = set(self.nodes)
        for node in post_nodes-pre_nodes:
            self.add_to_view(node, "master")
            self.add_to_view(node, view)
