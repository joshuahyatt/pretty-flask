# This file is a template, and might need editing before it works on your project.

FROM continuumio/miniconda3
ARG CI_JOB_TOKEN

WORKDIR /usr/src/app

COPY . .

RUN conda env create -n temp -f requirements.yml --force


CMD ["./startup.sh"]
EXPOSE 5000
EXPOSE 80