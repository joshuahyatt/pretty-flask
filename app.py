from flask import Flask, request, jsonify, render_template, redirect
from flask_wtf import FlaskForm
from wtforms import *
from utilities import dynaforms, debug
from utilities.broccoli import *
import json
import joblib
import networkx as nx

#move this into utilities
def get_form(jstr):
	fields=json.load(jstr)
	fg = dynaforms.FieldGenerator(fields)
	GenericForm = type("GenericForm", (object,), fg.formfields)
	return type('DynaForm', (FlaskForm, GenericForm), {})

app = Flask(__name__)
app.config.from_object(__name__)
#Make actual key secret later by using a config file
app.config['SECRET_KEY'] = '7d441a64345782354841f2b6176a'

@app.route("/")
def about():
	return render_template('general-template.html', my_list=[0,1,2,3,4,5], title="Main Page Title")

@app.route("/form/<filename>", methods=['GET','POST'])
def simple_form(filename):
	json_file = open("templates/"+filename)
	form_class = get_form(json_file)
	data = {}
	if request.method == 'POST':
		#Do validation and object saving here.
		for field in request.form:
			print(field, ': ', request.form[field])
		client_form = form_class(request.form)
	else:
		client_form = form_class()

	return render_template("dynamic-form.html", form=client_form, title="Generic Form Title")

def shutdown_server():
	func = request.environ.get('werkzeug.server.shutdown')
	if func is None:
		raise RuntimeError('Not running with the Werkzeug Server')
	func()

@app.route('/shutdown', methods=['GET','POST'])
def shutdown():
	debug.shutdown_server()
	return 'Server shutting down...'

@app.route('/graph', methods=['GET','POST'])
def render_graph():
	G = joblib.load("static/regressor.pkl")
	graph_json = json.dumps(nx.node_link_data(G))
	return render_template("graph-template.html", graph=graph_json)

model_dic = {"cluster":"2 clusters", "forest":"binary tree"}
@app.route('/predict/<model>', methods=['GET', 'POST'])
def api(model):
	return "You are using " + model_dic[model]

if __name__ == "__main__":
	#Load graph at the session level
	G = joblib.load("static/easy_graph.pkl")
	app.run(host = '0.0.0.0')
